Parsley + Spicelib sources with Flash Builder support
=====================================================

This is a tranformation of all the separate [Spicefactory repositories](https://github.com/spicefactory) into a single repository with Flash Builder project files added / included. Also, branches past 3.0.0 (which is an exact copy of Parsley / Spicelib at that version) contain modifications that, at the time of writing, were not backported to the original repositories and there was no plan to do so.


How to use it
-------------

  1. Clone this repository
  2. Check out a branch you're interested in. Branch 3.0.0 is the original Parsley / Spicelib library without modifications, branches 3.0.1 and newer contain fixes and improvements described in release notes below.
  3. Import the projects into Flash Builder.
  4. If you want to use these projects as dependencies of your other Flex projects, you still need to understand which projects to reference - please see section 2.2 of [the manual](http://www.spicefactory.org/parsley/docs/3.0/manual/). Project dependency graph looks like this:

![Parsley project dependencies (section 1.5 in the manual)](http://www.spicefactory.org/parsley/docs/3.0/manual/dependencies.png)

Projects in this repository represent the blue and red bubbles and are actually named the same (except of the casing) so it may help you to find out which projects you need. For example, in a Flex project that uses MXML configuration and no other, you will want to reference these projects:

  * Parsley-Core
  * Parsley-Flex
  * Spicelib-Commands
  * Spicelib-Reflect
  * Spicelib-Util

  
How this repository was created
--------------------------------

Probably the easiest way to describe what this repository exactly is is to describe the process of how it was created:

  * Copies of Spicelib / Parsley repositories were obtained on 27 Feb 2013. Each repository is now a subfolder in this repository, e.g. Spicelib-Reflect repository became a Spicelib-Reflect subfolder here. The versions obtained were:
    * Parsley-Core: 3.0.0
    * Spicelib-Command: 3.1.1
    * Spicelib-Logging: 3.0.0
    * Spicelib-Reflect: 3.0.0
    * Spicelib-Util: 3.1.0
    * Spicelib-XML-Mapper: 3.0.2
  * All the `.gitignore` files originally found in each repository were merged to a single top-level one.
  * The Parsley-Core repository was split into three subfolders because it in fact holds three different projects (see for instance a `build.xml` file where it creates three different SWCs). The resulting three subfolders were:
    * Parsley-Core
    * Parsley-Flex
    * Parsley-XML
  * Each subdirectory was converted to a Flash Builder project. More specifically:
    * All projects are Flex Library projects. Some need to be because they depend on the Flex framework (Parsley-Flex, Parsley-Core and Spicelib-Logging), the others so that the same SDK is used to compile them (Flash Builder uses a different SDK if the project is of type "ActionScript Library").
    * Dependecies have been set amongst them and compiled dependencies (files like spicelib-util.swc) were removed from the `libs` folders.
    * Compiler properties has been set so that AS3 metadata get preserved, XML manifests are compiled into the resulting SWCs etc.
    * Test files (usually in the `src\tests` folder) are still physicially present but not included in the source path.

    
Version history
-------------------

Note: some changes are described in more depth [on the project wiki](https://bitbucket.org/borekb/parsley-spicelib-complete/wiki/Home).

**3.0.1**

  * Logging output of commands made more useful
  * Bug fixes in CommandFlow
  * If a command inside a flow fails, the whole flow now fails too (used to finish as completed)
  * Added `metadata.xml` file that Flash Builder uses to hint on things like [Inject], [CommandResult] etc.

**3.0.0**

  * Created this repository as an exact copy of Parsley/Spicelib 3.0.0


Copyright, credits etc.
-----------------------

See Parsley's licence file for details and many thanks to Jens Halm for a great application framework. 